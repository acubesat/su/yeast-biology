## A playground for a yeast memory device based on Ajo-Franklin et al. 2007. Genes & development

# Import packages
import numpy as np
import matplotlib.pyplot as plt

# Set parameters
tau = 240
s = 0.01
n = 3.4
K = 60
beta = 9.3

# Define the production function 
def production(A_As):
    return s + beta * (A_As)**n / (K**n + (A_As)**n)

# Define the dilution function
def dilution(A_As):
    return (np.log(2) / tau) * A_As

# Generate a range of A + As values
A_As_range = np.linspace(0, 0.35e3, 10000) 

# Compute production and dilution for each value of A + As
production_values = [production(A_As) for A_As in A_As_range]
dilution_values = [dilution(A_As) for A_As in A_As_range]

# Plotting
plt.figure(figsize=(8, 4))
plt.plot(A_As_range/1000, np.array(production_values)/10, label='Production', color='black')  # convert to match graph units
plt.plot(A_As_range/1000, np.array(dilution_values), label='Dilution', color='orange')  # convert to match graph units
plt.xlabel('[A$_A$] + [A$_S$] ($10^6$ molecules cell$^{-1}$)')
plt.ylabel('d[A$_A$]/dt ($10^3$ molecules cell$^{-1}$ min$^{-1}$)')
plt.grid(True)
plt.legend()
plt.show()