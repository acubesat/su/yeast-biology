# Load libraries
library(dplyr)
library(tidyverse)
library(ggplot2)
library(wesanderson)
library(ggthemr)
library(viridis)

# Set paths
data_path <-
data_filename <- "20211117_KK_Data_annotated"

# Set wd
setwd(data_path)

# Load data
data <- read.csv(file.path(data_path, file.path(data_filename, "csv", fsep = ".")), header = TRUE, stringsAsFactors = FALSE)

# Calculate statistics
data_summary <- function(data, varname, groupnames){
  require(plyr)
  summary_func <- function(x, col){
    c(mean = mean(x[[col]], na.rm = TRUE),
      sd = sd(x[[col]], na.rm = TRUE))
  }
  data_sum<-ddply(data, groupnames, .fun = summary_func,
                  varname)
  data_sum <- rename(data_sum, c("mean" = varname))
  return(data_sum)
}

data_stats <- data_summary(data, varname = c("Colonies_Manual"),
                                          groupnames = c("State", "Ether", "Lyticase"))

# Plotting

data_plot <- data_stats

# Choose color palettes
gp1 <- wes_palettes$Darjeeling1
gp2 <- wes_palettes$Darjeeling2
ggthemr('fresh')

# Plotting
ggplot(data_plot, aes(x = as.factor(Lyticase), y = as.numeric(Colonies_Manual), fill = State)) +
    geom_bar(stat="identity", position = position_dodge()) +
    geom_errorbar(aes(ymin = Colonies_Manual - sd, ymax = Colonies_Manual + sd), width = .2,
                 position = position_dodge(.9), color = "black") +
    facet_grid(cols = vars(Ether)) +
    labs(x = "Lyticase (U/ml)", y = "Colony count") +
    theme(
        strip.text = element_text(face = "bold", color = viridis(1, begin = 0.5, end = 0.9, option = "D")[1]),
        legend.position = "bottom",
        legend.text = element_text(size = 16),  
        legend.title = element_text(size = 16),
        axis.text.x = element_text(angle = 60, hjust = 1, size = 16),
        axis.text.y = element_text(size = 16),
        axis.title.x = element_text(size = 20),
        axis.title.y = element_text(size = 20),
        panel.border = element_rect(color = "black", fill=NA, size=1),
        strip.text.x = element_text(face = "bold", size = 14, margin = margin(2, 2, 2, 2), hjust = 0.5, color = "azure4"),
        strip.text.y = element_text(face = "bold", size = 14, margin = margin(2, 2, 2, 2), hjust = 0.5, angle = -90, color = "azure4"),
        strip.background = element_rect(fill = '#FFFFFF',)
    ) +
    scale_color_manual(values = wes_palette(n = 3, name = "Darjeeling1")) +
    scale_fill_manual(values = wes_palette(n = 3, name = "Darjeeling1"))

# ggsave("20211117_Manual.pdf", width = 14, height = 8)

