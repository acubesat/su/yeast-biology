# Load libraries
library(dplyr)
library(tidyverse)
library(ggplot2)
library(wesanderson)
library(ggthemr)
library(viridis)

# Set paths
data_path <-
data_filename <- "20220511_DN_IK_Data"
design_filename <- "20220511_DN_IK_Design"

# Set timepoints
timepoints <- 34

# Set wd
setwd(data_path)

# Data preprocessing
source("data_preprocessing.R")
data_annotated <- data_preprocessing(data_path, data_filename, design_filename, timepoints)

# Select data for downstream analysis-plotting
data_analysis <- filter(data_annotated, Medium %in% "SD-His-Leu")

# Create df with OD of the blank media
data_blank <- filter(data_analysis,
(Strain %in% "Blank" & Replicate %in% "1"))

# Subtract OD of blank media from samples (to correct for medium absorbance)
data_analysis %>% group_by(Well, Replicate, Strain, State, Medium) %>%
summarize(Time = Time, OD_corrected = 
as.numeric(OD) - as.numeric(data_blank$OD)) -> data_analysis_corrected

# Calculate statistics
data_summary <- function(data, varname, groupnames){
  require(plyr)
  summary_func <- function(x, col){
    c(mean = mean(x[[col]], na.rm = TRUE),
      sd = sd(x[[col]], na.rm = TRUE))
  }
  data_sum <- ddply(data, groupnames, .fun = summary_func,
                  varname)
  data_sum <- rename(data_sum, c("mean" = varname))
  return(data_sum)
}

data_stats <- data_summary(data_analysis_corrected, varname = c("OD_corrected"),
                                          groupnames = c("Time", "Strain", "State"))

# Renaming
data_stats$Strain[data_stats$Strain == "YHL033C-Y7039"] <- "YHL033C x Y7039"
data_stats$Strain[data_stats$Strain == "YIL066C-Y7039"] <- "YIL066C x Y7039"
data_stats$Strain[data_stats$Strain == "YML058W-A-Y7039"] <- "YML058W-A x Y7039"

# Factorize
data_stats$State <- factor(data_stats$State, levels = c("Liquid", "Dried"))

# Plotting

data_plot <- filter(data_stats, 
(Strain %in% "YHL033C x Y7039" | Strain %in% "YIL066C x Y7039" | Strain %in% "YML058W-A x Y7039"))

# Choose color palettes
gp1 <- wes_palettes$Darjeeling1
gp2 <- wes_palettes$Darjeeling2
ggthemr('fresh')

ggplot(data_plot, aes(x = as.numeric(Time), y = as.numeric(OD_corrected), group = State)) +
    geom_ribbon(aes(ymin = OD_corrected - sd, ymax = OD_corrected + sd, fill = State), alpha = 0.2) +
    geom_point(aes(color = State)) +
    # facet_grid(rows = vars(Strain), cols = vars(State)) +
    facet_grid(rows = vars(Strain)) +
    labs(x = "Time (hours)", y = expression(OD[600])) +
    theme(
        strip.text = element_text(face = "bold", color = viridis(1, begin = 0.5, end = 0.9, option = "D")[1]),
        legend.position = "bottom",
        legend.text = element_text(size = 16),  
        legend.title = element_text(size = 16),
        axis.text.x = element_text(angle = 60, hjust = 1, size = 16),
        axis.text.y = element_text(size = 16),
        axis.title.x = element_text(size = 20),
        axis.title.y = element_text(size = 20),
        panel.border = element_rect(color = "black", fill=NA, size=1),
        strip.text.x = element_text(face = "bold", size = 14, margin = margin(2, 2, 2, 2), hjust = 0.5, color = "azure4"),
        strip.text.y = element_text(face = "bold", size = 14, margin = margin(2, 2, 2, 2), hjust = 0.5, angle = -90, color = "azure4"),
        strip.background = element_rect(fill = '#FFFFFF',)
    ) +
    scale_color_manual(values = wes_palette(n = 3, name = "Darjeeling1")) +
    scale_fill_manual(values = wes_palette(n = 3, name = "Darjeeling1"))

# ggsave("20220511_DN_IK_YPD.pdf", width = 14, height = 8)

# Fitting

# Function to estimate parameters for a given state & strain
estimate_parameters <- function(data, state, strain) {
  # Filter the data for the specific state & strain
  state_data <- filter(data, State == state, Strain == strain)

  # Select the data for the exponential phase
  exp_phase_data <- if (state == "Liquid") {
    subset(state_data, Time >= 24 & Time <= 44)
  } else {
    subset(state_data, Time >= 34 & Time <= 48)
  }
    
  # Perform linear regression on the log-transformed OD values
  fit <- lm(log(OD_corrected) ~ Time, data = exp_phase_data)
  
  # Calculate μmax (maximum growth rate) as the slope of the regression line
  mu_max <- coef(fit)["Time"]
  
  # Calculate cell doubling time as ln(2) / μmax
  doubling_time <- log(2) / mu_max
  
  # Calculate lag time as the x-intercept of the regression line
  lag_time <- -coef(fit)["(Intercept)"] / mu_max
  
  # Return the calculated parameters
  return(c(mu_max = mu_max, doubling_time = doubling_time, lag_time = lag_time))
}

# Apply the function to each combination of strain and state
results <- list()

# Loop through each unique strain
for(strain in unique(data_plot$Strain)) {
  # Loop through each state
  for(state in unique(data_plot$State)) {
    # Filter data for current strain and state
    subset_data <- data_plot %>% filter(Strain == strain, State == state)
    
    # Calculate parameters and store results
    params <- estimate_parameters(data_plot, state, strain)
    results[[paste(strain, state, sep = "_")]] <- params
  }
}

# Convert the list of results to a data frame for easier viewing
results_df <- do.call(rbind, results) %>% 
  as.data.frame() %>%
  rownames_to_column(var = "Strain_State")

# Rename columns
colnames(results_df)[2] = "mu_max"
colnames(results_df)[3] = "doubling_time"
colnames(results_df)[4] = "lag_time"

# Print the results
print(results_df)

# Plotting the fits

# Create a dataframe for the line data
lines_data <- results_df %>%
  mutate(
    Slope = as.numeric(as.character(mu_max)), # The slope of the line
    Intercept = as.numeric(as.character(lag_time)) # Use lag_time * Slope if Intercept is not present
  ) %>%
  separate(Strain_State, into = c("Strain", "State"), sep = "_", remove = TRUE) %>%
  select(Strain, State, Intercept, Slope)

# Create a dataframe for the line points
line_points <- do.call(rbind, lapply(1:nrow(lines_data), function(i) {
  # Create a sequence of time points from the start of the observed time range to the end
  time_seq <- seq(from = min(data_plot$Time), to = max(data_plot$Time), length.out = 100)
  
  # Calculate the OD_corrected values only after the lag time, before that it should be zero
  od_corrected <- ifelse(time_seq >= lines_data$Intercept[i],
                         lines_data$Slope[i] * (time_seq - lines_data$Intercept[i]),
                         0)
  
  data.frame(
    Time = time_seq,
    OD_corrected = od_corrected,
    Strain = lines_data$Strain[i],
    State = lines_data$State[i]
  )
}))

# Plot the fits together with the raw data
ggplot(data_plot, aes(x = Time, y = OD_corrected, color = State)) +
  geom_ribbon(aes(ymin = OD_corrected - sd, ymax = OD_corrected + sd, fill = State), alpha = 0.2) +
  geom_point(aes(color = State)) +
  geom_line(data = line_points, aes(group = interaction(Strain, State)), linetype = "dashed") +
    facet_grid(rows = vars(Strain)) +
    labs(x = "Time (hours)", y = expression(OD[600])) +
    theme(
        strip.text = element_text(face = "bold", color = viridis(1, begin = 0.5, end = 0.9, option = "D")[1]),
        legend.position = "bottom",
        legend.text = element_text(size = 16),  
        legend.title = element_text(size = 16),
        axis.text.x = element_text(angle = 60, hjust = 1, size = 16),
        axis.text.y = element_text(size = 16),
        axis.title.x = element_text(size = 20),
        axis.title.y = element_text(size = 20),
        panel.border = element_rect(color = "black", fill=NA, size=1),
        strip.text.x = element_text(face = "bold", size = 14, margin = margin(2, 2, 2, 2), hjust = 0.5, color = "azure4"),
        strip.text.y = element_text(face = "bold", size = 14, margin = margin(2, 2, 2, 2), hjust = 0.5, angle = -90, color = "azure4"),
        strip.background = element_rect(fill = '#FFFFFF',)
    ) +
    scale_color_manual(values = wes_palette(n = 3, name = "Darjeeling1")) +
    scale_fill_manual(values = wes_palette(n = 3, name = "Darjeeling1")) +
    ylim(0,2)