# Load libraries
library(dplyr)
library(tidyverse)
library(ggplot2)
library(ggthemr)
library(viridis)

# Set paths
data_path <-
data_filename <-
design_filename <-

# Set timepoints
timepoints <- 31

# Set wd
setwd(data_path)

# Data preprocessing
source("data_preprocessing.R")
data_annotated <- data_preprocessing(data_path, data_filename, design_filename, timepoints)

# Rename values
data_annotated[data_annotated == "Prespo_3D_Spo"] <- "20221124 (P3, S)"
data_annotated[data_annotated == "Prespo_1D_Spo"] <- "20221124 (P1, S)"
data_annotated[data_annotated == "Spo"] <- "20221124 (S)"

# Select data for downstream analysis-plotting
data_analysis_samples <- filter(data_annotated, 
(State %in% c("Dried") & Medium %in% c("SD-His")))

# Create df with OD of the blank media
data_blank_no <- filter(data_annotated,
(Strain %in% c("Blank") & Replicate %in% c("1") & Medium %in% c("SD-His")))

# Subtract OD of blank media from samples (to correct for medium absorbance)
data_analysis_samples %>% group_by(Well, Replicate, Strain, Method, State, Medium) %>%
summarize(Time = Time, OD_corrected = 
as.numeric(OD) - as.numeric(data_blank_no$OD)) -> data_analysis_samples_corrected

# Save dataframe with corrected data as .csv
write.csv(data_analysis_samples_corrected, paste0(data_filename, "_corrected_SD_His.csv"))

# Plotting
ggthemr('fresh')

ggplot(data_analysis_samples_corrected, aes(x = as.numeric(Time), y = as.numeric(OD_corrected)))+
    geom_point(aes(color = Strain)) +
    # geom_smooth(aes(color = Strain), se = FALSE) +
    theme_bw(base_size = 25) +
    facet_wrap(~Method) +
    labs(x = "Time (hours)", y = expression(OD[600])) +
    theme(
        strip.text = element_text(face = "bold", color = viridis(1, begin = 0.5, end = 0.9, option = "D")[1]),
        legend.position = "bottom",
        legend.text = element_text(size = 12),  
        legend.title = element_text(size = 14),
        axis.text.x = element_text(angle = 60, hjust = 1, size = 14),
        axis.text.y = element_text(size = 14),
        axis.title.x = element_text(size = 20),
        axis.title.y = element_text(size = 20),
        panel.border = element_rect(color = "black", fill=NA, size=1),
        strip.text.x = element_text(face = "bold", size = 16, margin = margin(2, 2, 2, 2), hjust = 0.5, color = "azure4"),
        strip.text.y = element_text(face = "bold", size = 16, margin = margin(2, 2, 2, 2), hjust = 0.5, angle = -90, color = "azure4"),
        strip.background = element_rect(fill = '#FFFFFF',)
    ) +
    scale_fill_viridis(discrete = TRUE) +
    scale_color_viridis(discrete = TRUE)

# ggsave("20230502_AA_Dried_SD_His.pdf", width = 14, height = 8)